export EDITOR=nvim
export VISUAL=nnnedit
export TERMCMD=st
LESS="${LESS:+$LESS }-c"
export LESS

## nnn
export NNN_PLUG='t:!&st*;g:!&gimp "$nnn"*'
export NNN_USE_EDITOR=1
export NNN_OPENER=nnnopen
export NNN_COLORS='1267'
export NNN_OPTS="R"

## game ting
export __GL_SHADER_DISK_CACHE_SKIP_CLEANUP=1
export __GL_SHADER_DISK_CACHE=1
export PROTON_FORCE_LARGE_ADDRESS_AWARE=1
#export DXVK_CONFIG_FILE=/home/drew/.config/dxvk.conf

#export FREETYPE_PROPERTIES="cff:no-stem-darkening=0 autofitter:no-stem-darkening=0"

## qt5 fix
#export QT_QPA_PLATFORMTHEME="qt5ct"

### wayland
#export QT_QPA_PLATFORM=wayland
#export XDG_CURRENT_DESKTOP=sway
#export XDG_SESSION_DESKTOP=sway
#export XDG_CURRENT_SESSION_TYPE=wayland
#export GDK_BACKEND="wayland,x11"
#export MOZ_ENABLE_WAYLAND=1
## cursor size fix
#export XCURSOR_SIZE=24
## bemenu
#export BEMENU_OPTS="--fn 'IBM Plex Mono 11' --hp 10 --nb '#101010ff' --nf '#f0ebe3ff' --ab '#101010ff' --af '#f0ebe3ff' --tb '#24a7a0ff' --tf '#ffffffff' --hb '#101010ff' --hf '#ff0064ff' --fbb '#101010ff' --fbf '#f0ebe3ff' --sb '#101010ff' --sf '#f0ebe3ff' --fb '#101010ff' --ff '#f0ebe3ff'"
#export BEMENU_BACKEND=wayland

## pipewire
export PULSE_LATENCY_MSEC=83
export PIPEWIRE_LATENCY=1024/48000

