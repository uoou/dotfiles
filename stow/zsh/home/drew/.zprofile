path=(~/.local/bin ~/.local/bin/fp $path)
#if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then

# xorg
if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
  exec startx
fi

## wayland
#if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
#	#proptest -M i915 -D /dev/dri/card0 95 connector 97 1 &&
#	exec Hyprland
#	#/usr/lib/plasma-dbus-run-session-if-needed /usr/bin/startplasma-wayland
#fi
