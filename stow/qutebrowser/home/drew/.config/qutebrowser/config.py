config.load_autoconfig(True)

c.content.notifications.enabled = True
c.content.blocking.method = 'both'
c.content.blocking.adblock.lists = ['https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters.txt', 'https://easylist.to/easylist/easylist.txt', 'https://easylist.to/easylist/easyprivacy.txt', '/home/drew/.config/qutebrowser/lol.txt']
#c.content.blocking.hosts.lists = ['https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts', '/home/drew/.config/qutebrowser/block_hosts.txt'] # (moved to system hosts file)
c.content.cookies.accept = 'no-3rdparty'
c.content.tls.certificate_errors = 'load-insecurely'
c.content.blocking.enabled = True
c.content.dns_prefetch = False
c.content.local_content_can_access_remote_urls = True

# variables
darkgrey = "#040404"
lessdarkgrey = "#3e3e3e"
midgrey = "#808080"
white = "#f0ebe3"
magenta = "#ff0064"
cyan = "#2ec3bb"
yellow = "#f3bf1d"
red = "#ff821b"

startpage = "file:/home/drew/Documents/begin.html"

# javascript off (toggle with tSH (S for permanent, H for subdomains)
c.content.javascript.enabled = True

# ui n ting
c.auto_save.session = True
c.content.autoplay = False
c.statusbar.show = 'always'
c.fonts.default_family = 'IBM Plex Mono'
c.fonts.default_size = "11pt"
c.messages.timeout = 5000
c.content.fullscreen.overlay_timeout = 0
c.editor.command = ['kitty', '-e', 'vim', '{file}', '-c', 'normal {line}G{column0}l']
c.scrolling.bar = 'never'
c.completion.open_categories = ['quickmarks', 'searchengines', 'history']
c.url.start_pages = startpage
c.url.default_page = startpage
c.content.fullscreen.window = True
c.spellcheck.languages = ['en-GB']
c.content.user_stylesheets = ['~/.config/qutebrowser/lol.css']
c.colors.webpage.preferred_color_scheme = 'dark'
c.content.prefers_reduced_motion = True

# tabs
c.tabs.background = True
c.tabs.last_close = 'startpage'
c.tabs.show = 'always'
c.tabs.favicons.scale = 1
c.tabs.indicator.padding = {"top": 0, "right": 0, "bottom": 0, "left": 0}
c.tabs.position = "left"
c.tabs.title.format = ''
c.tabs.width = 35
c.tabs.indicator.width = 3
c.tabs.indicator.padding = {"top": 0, "right": 3, "bottom": 0, "left": 0}
c.tabs.padding = {"top": 3, "right": 0, "bottom": 3, "left": 5}


# keys
config.bind('<Ctrl-e>', 'open-editor')
config.bind('M', 'spawn mpv --force-window=immediate --volume=50 {url}')
config.bind('C', 'hint images run spawn sxivu {hint-url}')
config.bind('<Ctrl-m>', 'hint links spawn mpv --force-window=immediate --volume=50 {hint-url}')
config.bind('zp', 'spawn --userscript ~/.config/qutebrowser/password_fill')
config.bind('<Ctrl-s>', 'config-cycle -t tabs.show always never')
config.bind('<Ctrl-b>', 'config-cycle -t scrolling.bar always never')
config.bind('b', 'config-cycle -t statusbar.show always never')
config.bind('zi', 'hint inputs')
config.bind('<Ctrl-Shift-f>', 'hint links tab-fg')
config.bind('E', 'config-edit')
config.bind('e', 'set-cmd-text -s :tab-select')
config.bind('X', 'hint images run open -t {hint-url}')
config.bind('<Ctrl-h>', 'open -t qute://history/')
config.bind('<Ctrl-Shift-h>', 'open -t file:///home/drew/Documents/begin.html')

# lists
config.bind('<Ctrl-l>', 'spawn lists {title}')
config.bind('<Ctrl-k>', 'spawn mem {primary}')

# hw acceleration maybe
#config.set('qt.args', ['ignore-gpu-blocklist', 'enable-gpu-rasterization', 'enable-accelerated-video-decode', 'enable-quic'])
#c.qt.args += [
#    "ignore-gpu-blacklist",
#    "enable-accelerated-2d-canvas",
#    "enable-gpu-memory-buffer-video-frames",
#    "enable-gpu-rasterization",
#    "enable-native-gpu-memory-buffers",
#    "enable-oop-rasterization",
#    "enable-zero-copy",
#]

# pdf
c.content.pdfjs = True

# search
c.url.open_base_url = True
c.url.searchengines = {
"DEFAULT": "https://www.google.co.uk/search?&q={}",
"gg": "https://www.google.co.uk/search?&q={}",
"ggi": "https://www.google.co.uk/search?q={}&tbm=isch",
"w": "https://en.wikipedia.org/w/index.php?search={}",
"st": "http://store.steampowered.com/search/?term={}",
"g": "https://duckduckgo.com/?q={}",
"mw": "http://en.uesp.net/w/index.php?title=Special%3ASearch&search={}",
"aur": "https://aur.archlinux.org/packages/?O=0&K={}",
"pac": "https://www.archlinux.org/packages/?sort=&arch=x86_64&maintainer=&flagged=&q={}",
"nh": "https://nethackwiki.com/wiki/index.php?search={}",
"aw": "https://wiki.archlinux.org/index.php?title=Special%3ASearch&search={}",
"gw": "https://wiki.gentoo.org/index.php?title=Special%3ASearch&search={}&go=Go",
"i": "http://www.imdb.com/find?ref_=nv_sr_fn&s=all&q={}",
"dick": "https://en.wiktionary.org/wiki/{}",
"ety": "http://www.etymonline.com/index.php?allowed_in_frame=0&search={}",
"u": "http://www.urbandictionary.com/define.php?term={}",
"y": "https://www.youtube.com/results?search_query={}",
"it": "https://itch.io/search?q={}",
"tpb": "https://www.magnetdl.com/search/?m=1&q={}",
"gi": "https://duckduckgo.com/?q={}&iar=images",
"p": "https://www.protondb.com/search?q={}",
"a": "https://smile.amazon.co.uk/s/?url=search-alias&field-keywords={}",
"gt": "https://translate.google.com/#view=home&op=translate&sl=auto&tl=en&text={}",
"tr": "https://translate.google.com/#view=home&op=translate&sl=en&tl=ru&text={}",
"fr": "https://translate.google.com/#view=home&op=translate&sl=ru&tl=en&text={}",
"rt": "https://www.rottentomatoes.com/search?search={}",
"g2": "https://www.g2play.net/listing?phrase={}",
}

# styling
c.colors.completion.category.bg = darkgrey
c.colors.completion.category.border.bottom = white
c.colors.completion.category.border.top = darkgrey
c.colors.completion.category.fg = white
c.colors.completion.even.bg = darkgrey
c.colors.completion.odd.bg = darkgrey
c.colors.completion.fg = white
c.colors.completion.item.selected.border.bottom = darkgrey
c.colors.completion.item.selected.border.top = darkgrey
c.colors.completion.item.selected.bg = cyan
c.colors.completion.item.selected.fg = darkgrey
c.colors.completion.item.selected.match.fg = magenta
c.colors.completion.match.fg = magenta 

c.completion.scrollbar.width = 0

c.colors.contextmenu.menu.bg =  lessdarkgrey
c.colors.contextmenu.menu.fg =  white
c.colors.contextmenu.disabled.bg = lessdarkgrey 
c.colors.contextmenu.disabled.fg = midgrey
c.colors.contextmenu.selected.bg = midgrey
c.colors.contextmenu.selected.fg = darkgrey

# hints
c.colors.hints.bg = yellow
c.colors.hints.fg =  darkgrey
c.colors.hints.match.fg = red
c.hints.radius = 0
c.hints.border = "1px solid #647e00"

c.colors.tabs.bar.bg = darkgrey
c.colors.tabs.even.bg = darkgrey
c.colors.tabs.odd.bg = darkgrey
c.colors.tabs.pinned.even.bg = darkgrey
c.colors.tabs.pinned.odd.bg = darkgrey
c.colors.tabs.selected.even.fg = magenta
c.colors.tabs.selected.odd.fg = magenta
c.colors.tabs.selected.even.bg = lessdarkgrey
c.colors.tabs.selected.odd.bg = lessdarkgrey
c.colors.tabs.pinned.selected.even.bg = lessdarkgrey
c.colors.tabs.pinned.selected.odd.bg = lessdarkgrey
c.colors.tabs.pinned.selected.even.fg = magenta
c.colors.tabs.pinned.selected.odd.fg = magenta

c.content.cache.size = 2147483647

c.colors.tabs.indicator.start = lessdarkgrey
c.colors.tabs.indicator.stop = midgrey
c.colors.tabs.indicator.error = magenta


