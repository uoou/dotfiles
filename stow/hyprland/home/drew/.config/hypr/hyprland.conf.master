# Please note not all available settings / options are set here.
# For a full list, see the wiki
#

# See https://wiki.hyprland.org/Configuring/Monitors/
$mon1=DP-1
$mon2=HDMI-A-1
monitor=,preferred,auto,auto
#monitor=$mon1, 2560x1440, 0x0, 1
#monitor=$mon2, 1920x1080, 2560x0, 1

workspace=1,monitor:$mon1
workspace=2,monitor:$mon1,default
workspace=3,monitor:$mon1
workspace=4,monitor:$mon1
workspace=5,monitor:$mon1
workspace=6,monitor:$mon1
workspace=8,monitor:$mon2,default
workspace=9,monitor:$mon2
workspace=10,monitor:$mon2


# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
# exec-once = waybar & hyprpaper & firefox
exec-once = swaybg -o $mon1 -i /home/drew/Pictures/Wallpapers/heidi/$(ls /home/drew/Pictures/Wallpapers/heidi/ | shuf | head -n1) -m fill & swaybg -o $mon2 -i /home/drew/Pictures/Wallpapers/heidi/$(ls /home/drew/Pictures/Wallpapers/heidi/ | shuf | head -n1) -m fill & kitty -T start & kitty -T start & qutebrowser & kitty -T comms neomutt & kitty -T files nnn -R & wl-paste --type text --watch cliphist store & wl-paste --type image --watch cliphist store & bassy & xwaylandvideobridge & upq

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# Some default env vars.
env = XCURSOR_SIZE,24
env = QT_QPA_PLATFORM,wayland
env = QT_WAYLAND_DISABLE_WINDOWDECORATION,1
env = GDK_BACKEND,wayland
#env = SDL_VIDEODRIVER,wayland
env = XDG_CURRENT_DESKTOP,Hyprland
env = XDG_SESSION_TYPE,wayland
env = XDG_SESSION_DESKTOP,Hyprland
# DRI
#env = WLR_DRM_DEVICES,/dev/dri/card0:/dev/dri/card1

misc {
  disable_hyprland_logo = true
}

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = us
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =

    follow_mouse = 1

    touchpad {
        natural_scroll = no
    }

    sensitivity = -0.7 # -1.0 - 1.0, 0 means no modification.
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 5
    gaps_out = 10
    border_size = 3
    col.active_border = rgba(ff007aff) rgba(00ffbfff) 45deg
    col.inactive_border = rgba(030303ff)

    layout = master
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 0
    blur = no
    blur_size = 3
    blur_passes = 1
    blur_new_optimizations = on

    drop_shadow = no
    shadow_range = 4
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
}

animations {
    enabled = yes

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 2, myBezier
    animation = windowsOut, 0, 7, default, popin 80%
    animation = windowsIn, 1, 1, default, popin 80%
    animation = border, 0, 10, default
    animation = borderangle, 0, 8, default
    animation = fade, 0, 7, default
    animation = workspaces, 1, 2, default
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mod + P in the keybinds section below
    preserve_split = yes # you probably want this
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    new_is_master = true
	no_gaps_when_only = 1
	new_on_top = true
	mfact = 0.5
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = off
}

# Example windowrule v1
# windowrule = float, ^(kitty)$
# Example windowrule v2
# windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
windowrulev2=workspace 1,^title:(start)$
windowrulev2=workspace 2,^class:(qutebrowser)$
windowrulev2=workspace 3,^title:(files)$
windowrulev2=workspace 4,^title:(comms)$
windowrulev2=workspace 5 silent,^class:(steam)$
windowrulev2=workspace 10 silent,^class:(discord)$
windowrulev2=float,^class:(pavucontrol)$
windowrulev2=tile,^class:(Nsxiv)$
windowrulev2=float,^title:(calc)$
windowrulev2=float,^title:(popterm)$
# pipewire thing for discord
windowrulev2 = opacity 0.0 override 0.0 override,class:^(xwaylandvideobridge)$
windowrulev2 = noanim,class:^(xwaylandvideobridge)$
windowrulev2 = nofocus,class:^(xwaylandvideobridge)$
windowrulev2 = noinitialfocus,class:^(xwaylandvideobridge)$

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mod = SUPER

# binds
# apps
bind = $mod SHIFT, Return, exec, kitty 
bind = $mod, R, exec, kitty -e nnn
bind = $mod CONTROL, Return, exec, kitty -T popterm
bind = $mod, Space, exec, wdecency_run
bind = $mod, P, exec, timepop
bind = $mod, U, exec, qutebrowser
bind = $mod, O, exec, discord
bind = $mod, C, exec, kitty -T calc calc
bind = $mod, I, exec, pavucontrol
bind = $mod, Y, exec, steam
bind = $mod, F, exec, scrup_screen 1
bind = $mod SHIFT, F, exec, scrup
bind = $mod, Z, exec, hyprpicker | wl-copy
bind = $mod, X, exec, authenticate_steam
bind = $mod, N, exec, passmenu
bind = $mod, B, exec, cliphist list | bemenu -l 20 -p clip | cliphist decode | wl-copy 

#bind = $mod, J, togglesplit, # dwindle

# master binds
bind = $mod, Return, layoutmsg, swapwithmaster
bind = $mod SHIFT, Q, exit, 
bind = $mod, W, killactive, 
bind = $mod, V, togglefloating
bind = $mod, Period, splitratio, exact 0.5,
bind = $mod, M, fullscreen
bind = $mod, H, focusmonitor, -1 
bind = $mod, L, focusmonitor, +1 
bind = $mod, K, layoutmsg, cycleprev
bind = $mod, J, layoutmsg, cyclenext
bind = $mod SHIFT, K, layoutmsg, swapprev
bind = $mod SHIFT, J, layoutmsg, swapnext
bind = $mod SHIFT, H, movewindow, mon:-1 
bind = $mod SHIFT, L, movewindow, mon:+1 

# Switch workspaces with mod + [0-9]
bind = $mod, 1, workspace, 1
bind = $mod, 2, workspace, 2
bind = $mod, 3, workspace, 3
bind = $mod, 4, workspace, 4
bind = $mod, 5, workspace, 5
bind = $mod, 6, workspace, 6
bind = $mod, 7, workspace, 7
bind = $mod, 8, workspace, 8
bind = $mod, 9, workspace, 9
bind = $mod, 0, workspace, 10

# Move active window to a workspace with mod + SHIFT + [0-9]
bind = $mod SHIFT, 1, movetoworkspacesilent, 1
bind = $mod SHIFT, 2, movetoworkspacesilent, 2
bind = $mod SHIFT, 3, movetoworkspacesilent, 3
bind = $mod SHIFT, 4, movetoworkspacesilent, 4
bind = $mod SHIFT, 5, movetoworkspacesilent, 5
bind = $mod SHIFT, 6, movetoworkspacesilent, 6
bind = $mod SHIFT, 7, movetoworkspacesilent, 7
bind = $mod SHIFT, 8, movetoworkspacesilent, 8
bind = $mod SHIFT, 9, movetoworkspacesilent, 9
bind = $mod SHIFT, 0, movetoworkspacesilent, 10

# Scroll through existing workspaces with mod + scroll
bind = $mod, mouse_down, workspace, e+1
bind = $mod, mouse_up, workspace, e-1

# Move/resize windows with mod + LMB/RMB and dragging
bindm = $mod, mouse:272, movewindow
bindm = $mod, mouse:273, resizewindow
bindm = $mod, mouse:274, togglefloating
