# See https://wiki.hyprland.org/Configuring/Monitors/
$mon1=DP-2
$mon2=HDMI-A-2
#monitor=,preferred,auto,auto
monitor=$mon1, preferred, 0x0, auto
monitor=$mon2, preferred, 2560x0, auto
#monitor=$mon2, disable
# DRI
env = WLR_DRM_DEVICES,/dev/dri/card0:/dev/dri/card1

$wob = /tmp/wobpipe

workspace=1,monitor:$mon1
workspace=2,monitor:$mon1,default
workspace=3,monitor:$mon1
workspace=4,monitor:$mon1
workspace=5,monitor:$mon1
workspace=6,monitor:$mon1
workspace=7,monitor:$mon1
workspace=8,monitor:$mon1
workspace=9,monitor:$mon1
workspace=10,monitor:$mon1
workspace=11,monitor:$mon2,default
workspace=12,monitor:$mon2
workspace=13,monitor:$mon2
workspace=14,monitor:$mon2
workspace=15,monitor:$mon2
workspace=16,monitor:$mon2
workspace=17,monitor:$mon2
workspace=18,monitor:$mon2
workspace=19,monitor:$mon2
workspace=20,monitor:$mon2
#workspace=special,allpseudo,false

# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
# exec-once = waybar & hyprpaper & firefox

exec-once = hyp_autostart
# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# Some default env vars.
env = XCURSOR_SIZE,24
env = QT_QPA_PLATFORM,wayland
# makes colours shit in qtwebengine
#env = QT_WAYLAND_DISABLE_WINDOWDECORATION,1
env = GDK_BACKEND,wayland
# breaks games
#env = SDL_VIDEODRIVER,wayland
env = XDG_CURRENT_DESKTOP,Hyprland
env = XDG_SESSION_TYPE,wayland
env = XDG_SESSION_DESKTOP,Hyprland

misc {
	disable_hyprland_logo = true
}

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = us
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =

    follow_mouse = 1

    touchpad {
        natural_scroll = no
    }

    sensitivity = -0.7 # -1.0 - 1.0, 0 means no modification.
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 7
    gaps_out = 14
    border_size = 5
    #col.active_border = rgba(dc2d54ff) rgba(aaaaaaff) rgba(0ed3a1ff) 45deg
	col.active_border = rgba(ff0064ff) rgba(ff821bff) rgba(f3bf1dff) rgba(a7de40ff) rgba(0adfa9ff) rgba(2ec3bbff) 30deg
    #col.active_border = rgba(ff007aff) rgba(aaaaaaff) rgba(00ffbfff) 45deg
	#col.active_border = rgba(ff007aff) rgba(00000000) rgba(00ffbfff) 45deg
    #col.active_border = rgba(ff664fff)
    #col.inactive_border = rgba(53002833) rgba(4a4a4a33) rgba(00533e33) 45deg
    col.inactive_border = rgba(040404ff)
    #col.active_border = rgba(422effff) rgba(53ff2eff) 45deg
    #col.inactive_border = rgba(030303ff)

    layout = dwindle
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 10
    blur {
        enabled = false
        size = 5
        passes = 3
    }

    drop_shadow = yes
    shadow_range = 0
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
}

animations {
    enabled = yes

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 2, myBezier
    animation = windowsOut, 0, 7, default, popin 80%
    animation = windowsIn, 1, 1, default, popin 80%
    animation = border, 0, 10, default
    animation = borderangle, 0, 8, default
    animation = fade, 0, 7, default
    animation = workspaces, 1, 2, default
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mod + P in the keybinds section below
    preserve_split = yes # you probably want this
	force_split = 2
	no_gaps_when_only = 1
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
	no_gaps_when_only = 1
	mfact = 0.5
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = off
}

# Example windowrule v1
# windowrule = float, ^(foot)$
# Example windowrule v2
# windowrulev2 = float,class:^(foot)$,title:^(foot)$
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
windowrulev2=workspace 1,^title:(start)$
windowrulev2=workspace 2,^class:(qutebrowser)$
windowrulev2=workspace 3,^title:(files)$
windowrulev2=workspace 4,^title:(comms)$
windowrulev2=workspace 5 silent,^class:(steam)$
windowrulev2=workspace 5 silent,^title:(Steam)$
windowrulev2=workspace 11 silent,^class:(discord)$
windowrulev2=workspace 1,^class:(zenity)$

windowrulev2=float,^class:(pavucontrol)$
windowrulev2=float,^class:(hl2_linux)$
windowrulev2=tile,^class:(Nsxiv)$
windowrulev2=float,^title:(calc)$
windowrulev2=float,^title:(popterm)$
#windowrulev2=workspace special,float,^title:(scratch)$
windowrulev2=float,^title:(scratch)$
# pipewire thing for discord
windowrulev2 = opacity 0.0 override 0.0 override,class:^(xwaylandvideobridge)$
windowrulev2 = noanim,class:^(xwaylandvideobridge)$
windowrulev2 = nofocus,class:^(xwaylandvideobridge)$
windowrulev2 = noinitialfocus,class:^(xwaylandvideobridge)$
#unrounding
windowrulev2 = rounding 0,class:^(steam)$,title:^()$

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mod = SUPER

# binds
# apps
bind = $mod SHIFT, Return, exec, foot
bind = $mod, R, exec, foot -e nnn
bind = $mod CONTROL, Return, exec, foot -T popterm
bind = $mod, Space, exec, pgrep bemenu || wdecency_run
bind = $mod, P, exec, timepop
bind = $mod, U, exec, qutebrowser
bind = $mod, O, exec, discord
bind = $mod, C, exec, foot -T calc calc
bind = $mod, I, exec, pavucontrol -t 1
bind = $mod, Y, exec, [silent] steam
bind = $mod, F, exec, scrup_screen 1
bind = $mod SHIFT, F, exec, scrup
bind = $mod, Z, exec, hyprpicker | wl-copy
bind = $mod, X, exec, authenticate_steam
bind = $mod, N, exec, pgrep bemenu || passmenu 
bind = $mod, B, exec, pgrep bemenu || cliphist list | bemenu -p "clip:" -l 20 | cliphist decode | wl-copy 
bind = $mod SHIFT, S, exec, mm 
bind = $mod, D, execr, vsee 2
#bind = $mod SHIFT, D, execr, vsee 1
bind = $mod, E, exec, pgrep bemenu || bemenu-emoji

# generic
bind = $mod SHIFT, Q, exit, 
bind = $mod, W, killactive, 
#bind = $mod, W, focuscurrentorlast,
bind = $mod, V, togglefloating
bind = $mod, M, fullscreen
#bind = $mod, BackSpace, togglespecialworkspace
#bind = $mod SHIFT, Tab, movetoworkspace, special
binde = $mod, bracketright, exec, pactl set-sink-volume @DEFAULT_SINK@ +2% && pactl get-sink-volume @DEFAULT_SINK@ | head -n 1 | awk '{print substr($5, 1, length($5)-1)}' > $wob
binde = $mod, bracketleft, exec, pactl set-sink-volume @DEFAULT_SINK@ -2% && pactl get-sink-volume @DEFAULT_SINK@ | head -n 1 | awk '{print substr($5, 1, length($5)-1)}' > $wob
bind = $mod, Return, bringactivetotop
bind = $mod, Period, splitratio, exact 0.5,
bind = $mod, A, pin
binde = $mod SHIFT CONTROL, K, resizeactive, 0 -30
binde = $mod SHIFT CONTROL, J, resizeactive, 0 30
binde = $mod SHIFT CONTROL, H, resizeactive, -30 0
binde = $mod SHIFT CONTROL, L, resizeactive, 30 0

## master binds
#bind = $mod, Return, layoutmsg, swapwithmaster master
#bind = $mod, H, focusmonitor, -1 
#bind = $mod, L, focusmonitor, +1 
#bind = $mod, K, layoutmsg, cycleprev
#bind = $mod, J, layoutmsg, cyclenext
#bind = $mod SHIFT, K, layoutmsg, swapprev
#bind = $mod SHIFT, J, layoutmsg, swapnext
#bind = $mod SHIFT, H, movewindow, mon:-1 
#bind = $mod SHIFT, L, movewindow, mon:+1 
#bind = $mod, Tab, layoutmsg, focusmaster master

# dwindle binds
bind = $mod, H, movefocus, l 
bind = $mod, L, movefocus, r 
bind = $mod, J, movefocus, d 
bind = $mod, K, movefocus, u 
bind = $mod SHIFT, K, swapwindow, u
bind = $mod SHIFT, J, swapwindow, d
bind = $mod SHIFT, H, swapwindow, l 
bind = $mod SHIFT, L, swapwindow, r 
bind = $mod CONTROL, K, movewindow, u
bind = $mod CONTROL, J, movewindow, d
bind = $mod CONTROL, H, movewindow, l 
bind = $mod CONTROL, L, movewindow, r 
bind = $mod, G, togglesplit, # dwindle

# Switch workspaces with mod + [0-9]
bind = $mod, 1, workspace, 1
bind = $mod, 2, workspace, 2
bind = $mod, 3, workspace, 3
bind = $mod, 4, workspace, 4
bind = $mod, 5, workspace, 5
bind = $mod, 6, workspace, 6
bind = $mod, 7, workspace, 7
bind = $mod, 8, workspace, 8
bind = $mod, 9, workspace, 9
bind = $mod, 0, workspace, 10
bind = ALT, 1, workspace, 11
bind = ALT, 2, workspace, 12
bind = ALT, 3, workspace, 13
bind = ALT, 4, workspace, 14
bind = ALT, 5, workspace, 15
bind = ALT, 6, workspace, 16
bind = ALT, 7, workspace, 17
bind = ALT, 8, workspace, 18
bind = ALT, 9, workspace, 19
bind = ALT, 0, workspace, 20

#bind = $mod, 1, layoutmsg, focusmaster master
#bind = $mod, 2, layoutmsg, focusmaster master
#bind = $mod, 3, layoutmsg, focusmaster master
#bind = $mod, 4, layoutmsg, focusmaster master
#bind = $mod, 5, layoutmsg, focusmaster master
#bind = $mod, 6, layoutmsg, focusmaster master
#bind = $mod, 7, layoutmsg, focusmaster master
#bind = $mod, 8, layoutmsg, focusmaster master
#bind = $mod, 9, layoutmsg, focusmaster master
#bind = $mod, 0, layoutmsg, focusmaster master
#bind = ALT, 1, layoutmsg, focusmaster master
#bind = ALT, 2, layoutmsg, focusmaster master
#bind = ALT, 3, layoutmsg, focusmaster master
#bind = ALT, 4, layoutmsg, focusmaster master
#bind = ALT, 5, layoutmsg, focusmaster master
#bind = ALT, 6, layoutmsg, focusmaster master
#bind = ALT, 7, layoutmsg, focusmaster master
#bind = ALT, 8, layoutmsg, focusmaster master
#bind = ALT, 9, layoutmsg, focusmaster master
#bind = ALT, 0, layoutmsg, focusmaster master

# Move active window to a workspace with mod + SHIFT + [0-9]
bind = $mod SHIFT, 1, movetoworkspacesilent, 1
bind = $mod SHIFT, 2, movetoworkspacesilent, 2
bind = $mod SHIFT, 3, movetoworkspacesilent, 3
bind = $mod SHIFT, 4, movetoworkspacesilent, 4
bind = $mod SHIFT, 5, movetoworkspacesilent, 5
bind = $mod SHIFT, 6, movetoworkspacesilent, 6
bind = $mod SHIFT, 7, movetoworkspacesilent, 7
bind = $mod SHIFT, 8, movetoworkspacesilent, 8
bind = $mod SHIFT, 9, movetoworkspacesilent, 9
bind = $mod SHIFT, 0, movetoworkspacesilent, 10
bind = SHIFT ALT, 1, movetoworkspacesilent, 11
bind = SHIFT ALT, 2, movetoworkspacesilent, 12
bind = SHIFT ALT, 3, movetoworkspacesilent, 13
bind = SHIFT ALT, 4, movetoworkspacesilent, 14
bind = SHIFT ALT, 5, movetoworkspacesilent, 15
bind = SHIFT ALT, 6, movetoworkspacesilent, 16
bind = SHIFT ALT, 7, movetoworkspacesilent, 17
bind = SHIFT ALT, 8, movetoworkspacesilent, 18
bind = SHIFT ALT, 9, movetoworkspacesilent, 19
bind = SHIFT ALT, 0, movetoworkspacesilent, 20

# Scroll through existing workspaces with mod + scroll
#bind = $mod, mouse_down, workspace, e+1
#bind = $mod, mouse_up, workspace, e-1

# Move/resize windows with mod + LMB/RMB and dragging
bindm = $mod, mouse:272, movewindow
bindm = $mod, mouse:273, resizewindow
bindm = $mod, mouse:274, togglefloating

