#!/usr/bin/env bash

# https://gitlab.com/uoou/dotfiles/-/tree/master/stow/bin/home/drew/.local/bin/gemb

[[ "$#" -ne 1 ]] && echo "needs URL" && exit

cyan="[36m"
magenta="[35m"
green="[32m"
yellow="[33m"
red="[31m"
blue="[34m"
bold="[1m"
nc="[m"
esc=$(printf "\033")

h1_col="$red"
h2_col="$green"
h3_col="$cyan"
link_col="$cyan"
external_link_col="$blue"
code_col="$magenta"

tmpdir=$(mktemp -d -t gemb-XXXXXXXXXX)
trap '{ rm -rf -- "$tmpdir"; }' EXIT

url="$1"

declare -a links
declare -a history
histpos=-1
dohist=true
first=true
getpage () {
	count=1
	page=""
	if [[ "$url" != *://* ]];then
		domain="$url"
		url="gemini://$url"
	else
		domain=$(sed 's/gemini:\/\///' <<< "$url")
	fi
	if [[ "$url" != *"."*"/"*"."* ]] && [[ $url != *"/" ]];then
		url="$url/"
	fi
	domain=$(sed 's/\/.*$//' <<< "$domain")

	if [[ "$url" == "http"*"://"* ]];then
		nohup xdg-open "$url" > /dev/null 2>&1
		echo "opening in web browser probably"
		url="$oldurl"
		domain="$olddomain"
		readlink
	fi

	if [[ "$url" == "gemini://"*"/"*"."* ]] && [[ "$url" != *".gmi" ]] && [[ "$url" != *".txt" ]];then
		filename=$(sed 's/.*\/\([^\/]*\.[^\/]*\)$/\1/' <<<"$url")
		echo "downloading..."
		echo "file will open in default application"
		openssl s_client -crlf -quiet -connect "$domain:1965" <<<"$url" 2>/dev/null | sed '1 d' > "$tmpdir/$filename" &&
		xdg-open "$tmpdir/$filename" &
		url="$oldurl"
		domain="$olddomain"
		readlink
	fi

	rawpage=$(openssl s_client -crlf -quiet -connect "$domain:1965" <<<"$url" 2>/dev/null | sed '1d')
	if [[ "$rawpage" == "\n" ]] || [[ "$rawpage" == "" ]];then
		echo "nowt there, like"
		url="$oldurl"
		domain="$olddomain"
		readlink
	fi

	if [[ "$dohist" = true ]];then
		while [[ $histpos -lt $((${#history[@]} - 1)) ]];do
			unset history[${#history[@]}-1]
		done
		history[${#history[@]}]="$url"
		histpos=$((${#history[@]} - 1))
	fi

	links=()
	while IFS= read -r i;do
		if [[ $count -lt 10 ]];then
			spaces="  "
		elif [[ $count -lt 100 ]];then
			spaces=" "
		else
			spaces=""
		fi
		if [[ "$i" == "=>"* ]];then
			links[$count]=$(awk '{ print $2 }' <<<"$i")
			if [[ "$i" == "=> http"*"://"* ]] || [[ "$i" == "=> mailto:"* ]];then
				colour="$external_link_col"
			else
				colour="$link_col"
			fi
			if [[ "$i" == "=> "*" "* ]];then
				i=$(sed "s/=> [^[:space:]]*[[:space:]]*\(.*\)/${esc}$bold[$count]${esc}$nc$spaces${esc}$colour\1${esc}$nc/" <<<"$i")
			else 
				i=$(sed "s/^=> \(.*\)/${esc}$bold[$count]$spaces${esc}$nc${esc}$colour\1${esc}$nc/" <<<"$i")
			fi
			((count=count+1))
		fi
		page="$page$i\n"
	done <<<"$rawpage"

	printf "%$(tput cols)s\n"|tr ' ' '='
	echo -e "\n$page\n" | \
	sed -e "/^\`\`\`/,/^\`\`\`/ { /^\`\`\`$/d; s/^\`\`\`//g; s/\(.*\)/${esc}$code_col\1${esc}$nc/ }" | \
	sed -e "s/^\(#\s.*\)$/${esc}$bold${esc}$h1_col\1${esc}$nc/g" \
	-e "s/^\(##\s.*\)$/${esc}$bold${esc}$h2_col\1${esc}$nc/g" \
	-e "s/^\(###\s.*\)$/${esc}$bold${esc}$h3_col\1${esc}$nc/g" 
	readlink
}

sayhelps () {
	echo "${esc}${bold}help${esc}$nc: enter ${esc}${bold}link number${esc}$nc or ${esc}${bold}url${esc}$nc. ${esc}${bold}b${esc}$nc for back, ${esc}${bold}f${esc}$nc for forwards, ${esc}${bold}r${esc}$nc to reload, ${esc}${bold}q${esc}$nc to quit and ${esc}${bold}h${esc}$nc for this help"
}

followlink () {
	oldurl="$url"
	olddomain="$domain"
	if [[ "$link" == "b" ]];then
		if [[ $histpos -gt 0 ]];then
			histpos=$((histpos - 1))
			url=${history[histpos]}
			dohist=false
			getpage
		else
			echo "end of history"
			readlink
		fi
	elif [[ "$link" == "f" ]];then
		if [[ $histpos -lt $((${#history[@]} - 1)) ]];then
			histpos=$((histpos + 1))
			url=${history[histpos]}
			dohist=false
			getpage
		else
			echo "end of history"
			readlink
		fi
	elif [[ "$link" == "r" ]];then
		dohist=false
		getpage
	elif [[ "$link" =~ ^[0-9]+$ ]];then
		if [[ $link -gt ${#links[@]} ]];then
			echo "no such link"
			readlink
		fi
		newlink=${links[$link]}
		if [[ "$newlink" == "gemini://"* ]] || [[ "$newlink" == "http"*"://"* ]] || [[ "$newlink" == "mailto:"* ]];then
			url="$newlink"
		elif [[ "$newlink" == *"../"* ]];then
			url="$(sed -e 's/gemini:\/\///' -e 's/\/[^\/]*$/\//' <<<$url)"
			endbit="$(sed 's/.*\/\([^\/]*\)$/\1/' <<<$newlink)"
			while [[ "$newlink" == *"../"* ]];do
				newlink="$(sed 's/\.\.\///' <<<$newlink)"
				url="$(sed  -e 's/\/[^\/]*\/*$//' <<<$url)"
			done
			url="$url/$endbit"
		elif [[ "$newlink" == "/"* ]];then
			url="$domain$newlink"
		elif [[ "$newlink" == ".." ]];then
			url="$(sed -e 's/gemini:\/\///' -e 's/[^\/]*\.gmi//' -e 's/\/[^\/]*$//' -e 's/\/[^\/]*$/\//' <<<$url)"
		elif [[ "$newlink" == "./"* ]];then
			newlink="$(sed 's/^\.//' <<<$newlink)"
			url="$(sed -e 's/gemini:\/\///' -e 's/[^\/]*\.gmi//' -e 's/\/[^\/]*$//' <<<$url)$newlink"
		elif [[ "$newlink" == "."* ]];then
			newlink="$(sed 's/^\.//' <<<$newlink)"
			url="$(sed -e 's/gemini:\/\///' -e 's/[^\/]*\.gmi//' -e 's/\/[^\/]*$//' <<<$url)/$newlink"
		else [[ "$newlink" = *"."* ]]
			url="$(sed -e 's/gemini:\/\///' -e 's/[^\/]*\.gmi//' -e 's/\/[^\/]*$//' <<<$url)/$newlink"
		fi
		dohist=true
		getpage
	elif [[ "$link" == "h" ]] || [[ "$link" == "help" ]];then
		sayhelps
		readlink
	elif [[ "$link" == "q" ]];then
		rm -r "$tmpdir"
		exit 0
	else
		url="$link"
		dohist=true
		getpage
	fi
}

readlink () {
	if [[ "$first" = true ]];then
		sayhelps
		first=false
	fi
	echo -en "url: $url"
	echo -en "\nlink: "
	read link
	followlink
}

getpage


