## <a name="abt"></a> About the Author

ABOUT THE AUTHOR HERE

## <a name="cpy"></a> Copyright

Text copyright © 2017 AUTHOR NAME HERE

Cover image: IMAGE CREDIT HERE

This version is published under a Creative Commons Attribution-NonCommercial-NoDerivatives licence. This licence allows the content to be downloaded and shared with others, as long as attribution is credited to the original. The content may not be re-used commercially or altered in any way, including re-use of only extracts or parts. To view a copy of this licence, visit [here](http://creativecommons.org/licenses/by-nc-nd/3.0/).

## <a name="arg"></a> About Rounded Globe

Rounded Globe is a publishing venture situated on the border between scholarly research and the reading public. Our goal is to disseminate accessible scholarship beyond the borders of the academic world.

Accessibility has two sides: our ebooks are free from jargon and narrow disciplinary focus; and they are released under a legal license that allows readers to download an ebook for free if they cannot afford to purchase it.

For a list of our titles please visit our website, [roundedglobe.com](http://roundedglobe.com).
