# Account stuff

set smtp_pass = `pass show web/posteo.net | head -n 1`
set mbox_type=Maildir
set folder=/home/drew/.mail/posteo
set spoolfile=+Inbox
set header_cache=/home/drew/.mail/posteo/headers
set message_cachedir=/home/drew/.mail/posteo/bodies

set record = +Sent
set postponed = +Drafts
#set trash = +Trash
set from = uoou@posteo.net
set realname = 'Drew'
set use_from = yes
set smtp_url = smtp://uoou@posteo.net:$smtp_pass@posteo.de:587
set ssl_force_tls = yes
set ssl_starttls = yes

set timeout = 0
set mail_check = 1

unmailboxes *
mailboxes "=Sent" "=Drafts" "=Inbox"

#set new_mail_command = "/home/drew/bin/email_notify"
# doesn't seem to work any more

# General stuff

set text_flowed
unset mark_old
set delete           # don't ask, just do
unset confirmappend  # don't ask, just do!
set quit             # don't ask, just do!!

# bindings
bind index,pager g noop
bind index,pager gg noop
bind index,pager M noop
bind index,pager C noop
bind index,pager i noop
bind index \Cf noop

# sort/threading
set sort = reverse-date-received
#set sort     = threads
#set sort_aux = reverse-last-date-received
#set sort_re
#bind index - collapse-thread
#bind index _ collapse-all
#folder-hook . "exec collapse-all"
#macro   index,pager ">"     "<next-thread><previous-entry>"
#macro   index,pager "<"     "<previous-thread><next-entry>"

# look and feel
set pager_index_lines = 8
set pager_context     = 5
set pager_stop
set menu_scroll
set smart_wrap
set tilde
unset markers

# composing
set editor="vim +':set textwidth=0' +':set wrapmargin=0' +':set wrap'"
unset mime_forward

# headers and dates
ignore *                               # first, ignore all headers
unignore from: to: cc: date: subject:  # then, show only these
hdr_order from: to: cc: date: subject: # and in this order

bind index gg first-entry
macro index o "<shell-escape>mbsync -a<enter>" "run mailsync"

macro index,pager gi "<change-folder>=Inbox<enter>" "go to inbox" 
macro index,pager Mi "<save-message>=Inbox<enter>" "move mail to inbox" 
macro index,pager Ci "<copy-message>=Inbox<enter>" "copy mail to inbox" 
macro index,pager gs "<change-folder>=Sent<enter>" "go to sent" 
macro index,pager Ms "<save-message>=Sent<enter>" "move mail to sent" 
macro index,pager Cs "<copy-message>=Sent<enter>" "copy mail to sent" 
macro index,pager gd "<change-folder>=Drafts<enter>" "go to drafts" 
macro index,pager Md "<save-message>=Drafts<enter>" "move mail to drafts" 
macro index,pager Cd "<copy-message>=Drafts<enter>" "copy mail to drafts" 
macro index,pager gt "<change-folder>=Trash<enter>" "go to trash" 
macro index,pager Mt "<save-message>=Trash<enter>" "move mail to trash" 
macro index,pager Ct "<copy-message>=Trash<enter>" "copy mail to trash" 

set mailcap_path = /home/drew/.config/mutt/mailcap
set date_format="%d %b, %H:%M"
#set display_filter = "/home/drew/.local/bin/emaildates" # format times as local
set index_format="%5C  %-60.60s %-50.50F %> %D  %zs %?X?A& ?"
set query_command = "abook --mutt-query '%s'"
set rfc2047_parameters = yes
set sleep_time = 0		# Pause 0 seconds for informational messages
set markers = no		# Disables the `+` displayed at line wraps
set wait_key = no		# mutt won't ask "press key to continue"
set fast_reply			# skip to compose when replying
set fcc_attach			# save attachments with the body
set forward_format = "Fwd: %s"	# format of subject when forwarding
set forward_quote		# include message in forwards
set reverse_name		# reply as whomever it was to
set include			# include message in replies
auto_view text/html		# automatically show html (mailcap uses w3m)
auto_view application/pgp-encrypted
alternative_order text/plain text/enriched text/html

# General rebindings
bind attach <return> view-mailcap
bind attach l view-mailcap
bind editor <space> noop
bind index G last-entry
bind index gg first-entry
bind pager,attach h exit
bind pager l view-attachments
#bind index D delete-message
bind index d purge-message
bind index U undelete-message
bind index L limit
bind index h noop
bind index l display-message
bind browser h goto-parent
bind browser l select-entry
bind pager,browser gg top-page
bind pager,browser G bottom-page
bind index,pager,browser \Cd half-down
bind index,pager,browser \Cu half-up
bind index,pager R group-reply
bind index \031 previous-undeleted	# Mouse wheel
bind index \005 next-undeleted		# Mouse wheel
bind pager \031 previous-line		# Mouse wheel
bind pager \005 next-line		# Mouse wheel
bind editor <Tab> complete-query
bind index,pager H   "display-toggle-weed"

macro index,pager a "|abook --add-email\n" 'add sender to abook'
macro index \Cr "T~U<enter><tag-prefix><clear-flag>N<untag-pattern>.<enter>" "mark all messages as read"
macro index O "<shell-escape>mbsync -Va<enter>" "run mbsync to sync all mail"
macro index \Cf "<enter-command>unset wait_key<enter><shell-escape>read -p 'Enter a search term to find with notmuch: ' x; echo \$x >~/.cache/mutt_terms<enter><limit>~i \"\`notmuch search --output=messages \$(cat ~/.cache/mutt_terms) | head -n 600 | perl -le '@a=<>;chomp@a;s/\^id:// for@a;$,=\"|\";print@a'\`\"<enter>" "show only messages matching a notmuch pattern"
macro index A "<limit>all\n" "show all messages (undo limit)"


# Default index colors:
color index white default '.*'
color index_author red default '.*'
color index_number green default
color index_subject cyan default '.*'
color index_date blue default

# New mail is boldened:
color index brightwhite black "~N"
color index_author brightred black "~N"
color index_subject brightcyan black "~N"

# Deleted mail is dulled
color index brightblack default "~D"

# Other colors and aesthetic settings:
mono bold bold
mono underline underline
mono indicator reverse
mono error bold
color normal default default
color indicator brightblack white
color normal brightwhite default
color error red default
color tilde black default
color message cyan default
color markers red white
color attachment white default
color search brightmagenta default
color status brightwhite black
color hdrdefault brightgreen default
color quoted green default
color quoted1 blue default
color quoted2 cyan default
color quoted3 white default
color quoted4 red default
color quoted5 brightred default
color signature brightgreen default
color bold black default
color underline black default
color normal default default

# Regex highlighting:
color header blue default ".*"
color header brightmagenta default "^(From)"
color header brightcyan default "^(Subject)"
color header brightwhite default "^(CC|BCC)"
color body brightred default "[\-\.+_a-zA-Z0-9]+@[\-\.a-zA-Z0-9]+" # Email addresses
color body brightblue default "(https?|ftp)://[\-\.,/%~_:?&=\#a-zA-Z0-9]+" # URL
color body green default "\`[^\`]*\`" # Green text between ` and `
color body brightblue default "^# \.*" # Headings as bold blue
color body brightcyan default "^## \.*" # Subheadings as bold cyan
color body brightgreen default "^### \.*" # Subsubheadings as bold green
color body white default "^(\t| )*(-|\\*) \.*" # List items as white
color body brightcyan default "[;:][-o][)/(|]" # emoticons
color body brightcyan default "[;:][)(|]" # emoticons
color body brightcyan default "[ ][*][^*]*[*][ ]?" # more emoticon?
color body brightcyan default "[ ]?[*][^*]*[*][ ]" # more emoticon?
color body red default "(BAD signature)"
color body cyan default "(Good signature)"
color body brightblack default "^gpg: Good signature .*"
color body brightwhite default "^gpg: "
color body brightwhite red "^gpg: BAD signature from.*"
mono body bold "^gpg: Good signature"
mono body bold "^gpg: BAD signature from.*"
color body red default "([a-z][a-z0-9+-]*://(((([a-z0-9_.!~*'();:&=+$,-]|%[0-9a-f][0-9a-f])*@)?((([a-z0-9]([a-z0-9-]*[a-z0-9])?)\\.)*([a-z]([a-z0-9-]*[a-z0-9])?)\\.?|[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)(:[0-9]+)?)|([a-z0-9_.!~*'()$,;:@&=+-]|%[0-9a-f][0-9a-f])+)(/([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*(;([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*)*(/([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*(;([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*)*)*)?(\\?([a-z0-9_.!~*'();/?:@&=+$,-]|%[0-9a-f][0-9a-f])*)?(#([a-z0-9_.!~*'();/?:@&=+$,-]|%[0-9a-f][0-9a-f])*)?|(www|ftp)\\.(([a-z0-9]([a-z0-9-]*[a-z0-9])?)\\.)*([a-z]([a-z0-9-]*[a-z0-9])?)\\.?(:[0-9]+)?(/([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*(;([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*)*(/([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*(;([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*)*)*)?(\\?([-a-z0-9_.!~*'();/?:@&=+$,]|%[0-9a-f][0-9a-f])*)?(#([-a-z0-9_.!~*'();/?:@&=+$,]|%[0-9a-f][0-9a-f])*)?)[^].,:;!)? \t\r\n<>\"]"




